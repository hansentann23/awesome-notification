import 'package:awesome_notification_test/utilities.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';

import 'notifications.dart';

void main() {
  runApp(const MyApp());
  AwesomeNotifications().initialize('app_icon.png',
      [NotificationChannel(channelKey: 'basic_channel',
        channelName: 'Basic Notifications',
        defaultColor: Colors.teal,
        importance: NotificationImportance.High,
        channelShowBadge: true,
        channelDescription: 'Testing',
      ),

        NotificationChannel(channelKey: 'scheduled_channel',
            channelName: 'Scheduled Notifications',
            defaultColor: Colors.teal,
            locked: true,
            importance: NotificationImportance.High,
            channelDescription: 'Schedule'
        ),
      ]
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.teal,
          colorScheme: ColorScheme.fromSwatch().copyWith(secondary: Colors.tealAccent),
      ),
      title: 'Awesome Notification',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget{
  const HomePage ({Key? key}) : super (key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>{
  void initState(){
    super.initState();
    AwesomeNotifications().isNotificationAllowed().then((isAllowed){
      if(!isAllowed){
        showDialog(context: context, builder: (context) => AlertDialog(title: const Text('Allow Notifications'),
        content: const Text('Our app would like to send you a notifications'),
          actions: [TextButton(onPressed: (){Navigator.pop(context);
          },
              child: const Text(
            'Dont Allow',
                style: TextStyle(color: Colors.grey,
                    fontSize: 18),
          )),
            TextButton(onPressed: ()=> AwesomeNotifications()
                .requestPermissionToSendNotifications()
                .then((_) => Navigator.pop(context)),
                child: const Text('Allow',
              style: TextStyle(
                  color: Colors.teal,
                  fontSize: 18,
                  fontWeight: FontWeight.bold),)
            ),
          ],
        ),
        );
      }
    });

    AwesomeNotifications().createdStream.listen((notification) {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
          content: Text('Notification Created on ${notification.channelKey}',
          ),
      ));
    });
  }

  @override
  void dispose(){
    AwesomeNotifications().createdSink.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Awesome Notification"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(onPressed: createNotification, child : Text("Basic Notif")),
            ElevatedButton(onPressed: () async {
              NotificationWeekAndTime? pickedSchedule =
                  await pickSchedule(context);

              if(pickedSchedule != null){
                createScheduledNotification(pickedSchedule);
              }
            }, child : Text("Scheduled Notif")),
            ElevatedButton(onPressed: cancelScheduleNotifications, child : Text("Cancel Notif")),
          ],
        ),
      ),
    );
  }
}


