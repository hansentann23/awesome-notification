import 'package:awesome_notification_test/utilities.dart';
import 'package:awesome_notifications/awesome_notifications.dart';

Future<void> createNotification() async {
  await AwesomeNotifications().createNotification(
      content: NotificationContent(id: createUniqueId(),
          channelKey: 'basic_channel',
      title: '${Emojis.money_money_bag + Emojis.plant_cactus} Buy Plant Food!!!',
      body: 'Floris at 123 Main St. has 2 in stock',
        bigPicture: 'app_icon.png',
          notificationLayout: NotificationLayout.BigPicture,
      ),
  );
}

Future<void> createScheduledNotification(NotificationWeekAndTime notificationSchedule) async{
  await AwesomeNotifications().createNotification(
      content: NotificationContent(
          id: createUniqueId(),
          channelKey: 'scheduled_channel',
          title: '${Emojis.wheater_droplet} Add some water to your plant!',
        body: 'Water your plant regularly',
        notificationLayout: NotificationLayout.Default,
      ),
    actionButtons: [
      NotificationActionButton(
        key: 'MARK_DONE',
        label: 'Mark Done',
      ),
    ],
    schedule: NotificationCalendar(
      weekday: notificationSchedule.dayOfTheWeek,
      hour: notificationSchedule.timeOfDay.hour,
      minute: notificationSchedule.timeOfDay.minute,
      second: 0,
      millisecond: 0,
      repeats: true,
    ),
  );
}

Future<void> cancelScheduleNotifications()async{
  await AwesomeNotifications().cancelAllSchedules();
}